import re
import urllib.parse
import argparse

import pypandoc
import requests

from dataclasses import dataclass


@dataclass
class GoogleDoc:
    id: str
    name: str
    content: bytes
    format: str

    @property
    def filename(self) -> str:
        return re.sub(r"[^\w_\.]", "_", self.name.lower())


def download_google_doc(id: str, format: str) -> GoogleDoc:
    response = requests.get(
        f"https://docs.google.com/document/export?format={format}&id={id}&includes_info_params=true"
    )
    name = urllib.parse.unquote(
        re.search(
            r"filename\*=UTF-8''(.+)\..+", response.headers["content-disposition"]
        )[1]
    )
    return GoogleDoc(id, name, response.content, format)


def save_to_markdown(doc: GoogleDoc):
    pypandoc.convert_text(
        doc.content,
        "markdown_mmd",
        doc.format,
        outputfile=f"{doc.filename}.md",
        extra_args=[f"--extract-media={doc.filename}"],
    )


def main(args):
    format = "docx"
    doc = download_google_doc(args.id, format)
    save_to_markdown(doc)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert a public Google Doc to markdown."
    )
    parser.add_argument("id", help="the id of the document")
    args = parser.parse_args()
    main(args)
